import React from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './components/Login';
import Dashboard from './components/Dashboard';
import { BrowserRouter, Router, Route, Switch,Redirect } from 'react-router-dom';
import history from '../src/history';
import { connect } from 'react-redux';
import CreateTask from './components/CreateTask';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.requireAuth = this.requireAuth.bind(this);
  }
  requireAuth(url) {
    if (localStorage.jwtToken) {
      return <Redirect push to='/dashboard' />;
    }
    else{
      return <Redirect push to='/login' />; 
    }
    
  }
  render() {
    return (
      <div className="App">
        <Router history={history}>

          <div>
            <Switch>
              <Route path='/' exact render={this.requireAuth}/>
              <Route path="/login" render={()=>(
                  localStorage.jwtToken?(<Redirect push to='/'/>):(<Login />)
                )}  />
              <Route path="/dashboard" render={()=>(
                  localStorage.jwtToken?(<Dashboard/>):(<Redirect push to='/'/>)
                )} />
              <Route path="/create-task" render={()=>(
                  localStorage.jwtToken?(<CreateTask/>):(<Redirect push to='/'/>)
                )} />


            </Switch>
          </div>
        </Router>
      </div>
    );
  }

}

export default App;
