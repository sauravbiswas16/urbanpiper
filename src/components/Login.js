import React from 'react';
import { Link } from 'react-router-dom';
import { loginUser } from '../actions/auth';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { withRouter } from "react-router";
class Login extends React.Component {

    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            errors: {}
        };
    }
    onChange = e => {
        this.setState({ [e.target.name]: e.target.value });
        console.log(this.state)
    }
    onSubmit = e => {
        e.preventDefault();

        const userData = {
            username: this.state.username,
            password: this.state.password
        };

        this.props.loginUser(userData);
    }

    componentDidMount() {
        if (this.props.auth.isAuthenticated) {
            this.props.history.push('/dashboard');
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.isAuthenticated) {
            this.props.history.push('/dashboard');
        }

        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }

    render() {

        const { errors } = this.state;
        if (this.props.errors) {
            console.log(this.props.errors.response);
            // errors=this.props.errors.response;
        }
        return (
            <div className="login">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h1 className="display-4 text-center">Log In</h1>
                            <p className="lead text-center">
                                Sign in to your DevConnector account
                  </p>
                            <form onSubmit={this.onSubmit}>


                                <div className="form-group">
                                    <input
                                        type="text"
                                        className={classnames('form-control form-control-lg', {
                                            'invalid': errors.name
                                        })}
                                        placeholder="username"
                                        value={this.state.username}
                                        onChange={(e) => this.setState({ username: e.target.value })} />

                                    {errors.name && (<div className="invalid-feedback">{errors.name}</div>)}
                                </div>
                                <div className="form-group">
                                    <input
                                        type="password"
                                        className={classnames('form-control form-control-lg', {
                                            'invalid': errors.name
                                        })}
                                        placeholder="Password"
                                        value={this.state.password}
                                        onChange={(e) => this.setState({ password: e.target.value })} />
                                    {errors.name && (<div className="invalid-feedback">{errors.name}</div>)}
                                </div>


                                <input type="submit" className="btn btn-info btn-block mt-4" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default withRouter(connect(mapStateToProps, {
    loginUser
})(Login));