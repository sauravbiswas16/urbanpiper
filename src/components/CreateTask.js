import React from 'react';
import Navbar from './Navbar';
import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';

class CreateTask extends React.Component {
    state = {
        title: '',
        priority: '',
        errors: {}
    };

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
        console.log(this.state)
    }

    onSubmit = e => {
        e.preventDefault();

        const taskData = {
            title: this.state.title,
            priority: this.state.priority
        };

        setAuthToken(localStorage.getItem('jwtToken'));
        axios
            .post('/tasks', taskData)
            .then(res => {
                console.log(res.data);
            })
            .catch(err =>
                console.log(err)
            );
    }

    render() {
        return (
            <div>
                <Navbar />

                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h1 className="display-4 text-center">CreateTask</h1>

                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className='form-control form-control-lg'
                                        placeholder="Task Title"
                                        value={this.state.title}
                                        onChange={(e) => this.setState({ title: e.target.value })} />
                                </div>
                                <div className="form-group">
                                    <select className='form-control form-control-lg'
                                        value={this.state.priority}
                                        onChange={(e) => this.setState({ priority: e.target.value })} >
                                        <option value="0">Select</option>
                                        <option value="low">Low</option>
                                        <option value="medium">Medium</option>
                                        <option value="high">High</option>
                                    </select>

                                </div>


                                <input type="submit" className="btn btn-info btn-block mt-4" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateTask;